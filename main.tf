provider "aws" {
  region = "us-east-1"
}

module "vpc" {
  source = "./modules/vpc"
}

module "app" {
  source = "./modules/asg-elb-ec2"
  websg = "${module.vpc.websg}"
  public-subnets = "${module.vpc.public-subnets}"
  elbSg = "${module.vpc.elbSg}"
  mainVPC= "${module.vpc.mainVPC}"
  privateSubnet = "${module.vpc.privateSubnet}"
  appsg = "${module.vpc.appsg}"
  namePrefix = "${var.namePrefix}"
}
