terraform {
  backend "s3" {
    bucket = "alam-terraform-state"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

# data "terraform_remote_state" "SimpleMySQLApp" {
#   backend = "s3"
#   config = {
#     bucket = "alam-terraform-state"
#     key = "terraform.tfstate"
#     region = "us-east-1"
#   }
# }