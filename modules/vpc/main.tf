resource "aws_subnet" "public-subnets" {
  count =  2        # "${length(data.aws_availability_zones.azs.names)}"
  availability_zone = "${element(data.aws_availability_zones.azs.names,count.index)}"
  vpc_id = "${aws_vpc.mainVPC.id}"
  cidr_block = "${element(var.public_subnet_cidr,count.index)}"

  tags = {
      Name = "Public-Subnet-${count.index+1}"
  }
}

resource "aws_route_table" "publicRouteTable" {
  vpc_id = "${aws_vpc.mainVPC.id}"
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.default.id}"
  }
  tags = {
      Name = "Public Subnet Route Table"
  }
}

resource "aws_route_table_association" "publicRouteTableAssociation" {
  count = 2
  subnet_id = "${element(aws_subnet.public-subnets.*.id, count.index)}"
  route_table_id = "${aws_route_table.publicRouteTable.id}"
}



resource "aws_internet_gateway" "default" {
    vpc_id = "${aws_vpc.mainVPC.id}"
}

resource "aws_security_group" "websg" {
    name = "WebServerSecurityGroup"
    description = "Web Server Security Group"   
    vpc_id = "${aws_vpc.mainVPC.id}"
}

resource "aws_security_group_rule" "inboundSsh" {
  type            = "ingress"
  from_port       = 22
  to_port         = 22
  protocol        = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.websg.id}"

}
resource "aws_security_group_rule" "inboundHTTP" {
  type            = "ingress"
  from_port       = 80
  to_port         = 80
  protocol        = "tcp"
  security_group_id = "${aws_security_group.websg.id}"
  source_security_group_id = "${aws_security_group.elbSg.id}"
}

resource "aws_security_group_rule" "outboundAll" {
  type            = "egress"
  from_port       = 0
  to_port         = 65535
  protocol        = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.websg.id}"

}


# Private Subnet

resource "aws_subnet" "privateSubnet" {
  vpc_id = "${aws_vpc.mainVPC.id}"
  availability_zone = "us-east-1c"
  cidr_block = "93.0.6.0/24"

  tags = {
      Name = "Private-Subnet"
  }
}
# Private Security Group

resource "aws_security_group" "appsg" {
    name = "AppServerSecurityGroup"
    description = "App Server Security Group"   
    vpc_id = "${aws_vpc.mainVPC.id}"
}

resource "aws_security_group_rule" "inboundSSH" {
  type            = "ingress"
  from_port       = 22
  to_port = 22
  protocol        = "tcp"

  source_security_group_id = "${aws_security_group.websg.id}"
  security_group_id = "${aws_security_group.appsg.id}"

}
resource "aws_security_group_rule" "inboundMySQL" {
  type            = "ingress"
  from_port       = 3306
  to_port = 3306
  protocol        = "tcp"

  source_security_group_id = "${aws_security_group.websg.id}"
  security_group_id = "${aws_security_group.appsg.id}"

}



resource "aws_security_group_rule" "outboundAllPrivate" {
  type            = "egress"
  from_port       = 0
  to_port         = 65535
  protocol        = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.appsg.id}"

}


# Nat Gateway
resource "aws_eip" "eip" {}

resource "aws_nat_gateway" "natGateway" {
  count=1
  allocation_id = "${aws_eip.eip.id}"
  subnet_id = "${element(aws_subnet.public-subnets.*.id, count.index)}"
}

# allow internet access to Application nodes through nat #1

resource "aws_route_table" "natgwRouteTable" {
  count=1
  vpc_id = "${aws_vpc.mainVPC.id}"
  route {
      cidr_block = "0.0.0.0/0"
      nat_gateway_id = "${element(aws_nat_gateway.natGateway.*.id, count.index)}"
  }
  tags = {
      Name = "Nat Gateway Route Table"
  }
}

resource "aws_route_table_association" "natgwRouteTableAssocition" {
  count = 1
  route_table_id = "${element(aws_route_table.natgwRouteTable.*.id, count.index)}"
  subnet_id = "${element(aws_subnet.privateSubnet.*.id, count.index)}"
}


## Security Group for ELB
resource "aws_security_group" "elbSg" {
  name = "ELB-SecurityGroup"
  vpc_id = "${aws_vpc.mainVPC.id}"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
   from_port = 443
   to_port = 443
   protocol = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_vpc" "mainVPC" {
  cidr_block = "${var.vpc_cidr}"
  instance_tenancy = "default"
  enable_dns_hostnames = true
  tags = {
      Name = "MainVPC"
  }
}
