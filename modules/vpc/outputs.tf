output "websg" {
  value = "${aws_security_group.websg.id}"
}

output "public-subnets" {
  value = "${aws_subnet.public-subnets.*.id}"
}

output "elbSg" {
  value = "${aws_security_group.elbSg.id}"
}

output "mainVPC" {
  value = "${aws_vpc.mainVPC.id}"
}

output "privateSubnet" {
  value = "${aws_subnet.privateSubnet.id}"
}

output "appsg" {
  value = "${aws_security_group.appsg.id}"
}
