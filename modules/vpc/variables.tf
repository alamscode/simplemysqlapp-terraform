provider "aws" {
	region = "${var.region}"
}
variable "region" {
  default = "us-east-1"
}
variable "vpc_cidr" {
  default = "93.0.0.0/16"
}

variable "public_subnet_cidr" {
  type = "list"
  default = ["93.0.1.0/24","93.0.2.0/24", "93.0.3.0/24","93.0.4.0/24","93.0.5.0/24"]
}

variable "private_subnet_cidr" {
  type = "list"
  default = ["93.0.6.0/24"]
}

data "aws_availability_zones" "azs" {}

data "aws_ami" "amazon-linux-2" {
 most_recent = true
 filter {
   name   = "owner-alias"
   values = ["amazon"]
 }
 filter {
   name   = "name"
   values = ["amzn-ami-hvm*"]
 }
 owners = ["amazon"]
}