provider "aws" {
	region = "${var.region}"
}

variable "region" {
  default = "us-east-1"
}

data "aws_availability_zones" "azs" {}

data "aws_ami" "amazon-linux-2" {
 most_recent = true
 filter {
   name   = "owner-alias"
   values = ["amazon"]
 }
 filter {
   name   = "name"
   values = ["amzn-ami-hvm*"]
 }
 owners = ["amazon"]
}

variable "websg" {}
variable "public-subnets" {}
variable "elbSg" {}
variable "mainVPC" {}
variable "privateSubnet" {}
variable "appsg" {}
variable "namePrefix" {
    default = "alamsTerraform"
}


