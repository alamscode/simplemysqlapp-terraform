#!/bin/bash
set -x
sleep 30
yum install deltarpm -y
yum update -y
yum upgrade -y
yum install mysql-server -y 
yum install php -y
yum install --enablerepo="epel" ufw -y
yes | ufw enable 
ufw allow mysql
service mysqld start
mysqladmin -u root password test
mysqladmin -u root -ptest flush-privileges
mysql -u root -ptest -e "CREATE USER 'alam'@'%' IDENTIFIED BY '03325181228';"
mysql -u root -ptest -e "GRANT ALL PRIVILEGES ON * . * TO 'alam'@'%' IDENTIFIED BY '03325181228';"
mysql -u root -ptest -e "FLUSH PRIVILEGES;"
mysql -u root -ptest -e "CREATE DATABASE crud;"
mysql -u root -ptest << EOF 
use crud;
use crud;
CREATE TABLE users (id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
firstname VARCHAR(30) NOT NULL, 
lastname VARCHAR(30) NOT NULL, 
email VARCHAR(50) NOT NULL, 
age INT(3), 
location VARCHAR(50), 
date TIMESTAMP);
EOF
service mysqld restart