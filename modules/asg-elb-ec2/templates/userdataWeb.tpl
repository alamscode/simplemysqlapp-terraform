#!/bin/bash
set -x
yum install deltarpm -y
yum update -y
yum upgrade -y
yum install php-mysql mysql-sever httpd php -y
service httpd start
cd /var/www/
rm -rf html
wget https://s3.amazonaws.com/alamhtml/html.zip
unzip html.zip
rm -rf html.zip
sed -i -e 's/localhost/${private_ip}/g' /var/www/html/config.php