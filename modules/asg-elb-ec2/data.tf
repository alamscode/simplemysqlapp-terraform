data "template_file" "appServerUserData" {
  template = "${file("./modules/asg-elb-ec2/templates/userdataApp.tpl")}"
}

data "template_file" "webServerUserData" {
  template = "${file("./modules/asg-elb-ec2/templates/userdataWeb.tpl")}"
  vars = {
      private_ip = "${aws_instance.appServer.private_ip}"
  }
}