## Creating Launch Configuration
resource "aws_launch_configuration" "alamLC" {
  image_id               = "${data.aws_ami.amazon-linux-2.id}"
  instance_type          = "t2.micro"
  security_groups        = ["${var.websg}"]
  associate_public_ip_address = "true"
  user_data = "${data.template_file.webServerUserData.rendered}"
  lifecycle {
    create_before_destroy = true
  }
}

## Creating AutoScaling Group
resource "aws_autoscaling_group" "alamASG" {
  launch_configuration = "${aws_launch_configuration.alamLC.id}"
  vpc_zone_identifier = ["${element(var.public-subnets,0)}", "${element(var.public-subnets, 1)}"]
  min_size = 2
  max_size = 10
  target_group_arns = ["${aws_lb_target_group.elbTG.arn}"]
  health_check_type = "ELB"
  tag {
    key = "Name"
    value = "${var.namePrefix}-WebServer-AutoScalingGroup"
    propagate_at_launch = true
  }
}

### Creating ELB
resource "aws_lb" "webElb" {
  name = "${var.namePrefix}-WebServer-ELB"
  load_balancer_type = "application"
  security_groups = ["${var.elbSg}"]
  internal = "false"
  subnets = ["${element(var.public-subnets,0)}", "${element(var.public-subnets, 1)}"]
}

resource "aws_lb_target_group" "elbTG" {
  name = "${var.namePrefix}-ELB-TargetGroup"
  port = 80
  protocol = "HTTP"
  vpc_id = "${var.mainVPC}"
  target_type = "instance"
}

resource "aws_lb_listener" "lbListener" {
  load_balancer_arn = "${aws_lb.webElb.arn}"
  port = 80
  protocol = "HTTP"

  default_action  {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.elbTG.arn}"
  }
}


# Private Instance

resource "aws_instance" "appServer" {
  ami = "${data.aws_ami.amazon-linux-2.id}"
  subnet_id = "${var.privateSubnet}"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${var.appsg}"]
  associate_public_ip_address = false
  user_data = "${data.template_file.appServerUserData.rendered}"
}